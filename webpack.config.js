const os = require('os');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production' // true or false

const extractCSS = new MiniCssExtractPlugin({
    filename: '[name].css',
});

module.exports = {
    mode: isProd ? 'production' : 'development',

    context: path.resolve(__dirname, 'src/client'),

    resolve: {
        extensions: ['.js'],
        modules: [path.resolve(__dirname, 'src/client'), 'node_modules'],
    },

    entry: {
        app: 'app'
    },

    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/'
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: isProd
                    ? [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
                    : ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10 * 1024,
                            name: 'img/[name].[ext]'
                        }
                    },
                ],
            },
            {
                test: /\.(html)$/,
                use: 'html-loader'
            }
        ]
    },

    plugins: getPlugins(),

    performance: {
        hints: false,
    },

    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false,
                    },
                },
                parallel: os.cpus().length,
            }),

            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: { discardComments: { removeAll: true } },
            }),
        ],
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        open: true
    },
}

function getPlugins() {
    const plugins = [
        new HtmlWebpackPlugin({
            title: 'Project Demo',
            template: path.resolve(__dirname, 'src/client/index.html'),
        })
    ];

    if (isProd) {
        plugins.push(extractCSS);
    } else {
        plugins.push(new webpack.HotModuleReplacementPlugin())
    }

    return plugins;
}

